<?php require_once('config.php') ?>
<?php require_once('includes/head_section.php') ?>
<?php require_once('includes/public_functions.php') ?>
<?php require_once('includes/registration_login.php') ?>
<!-- Retrieve all posts from database  -->
<?php $posts = getPublishedPosts(); ?>
<!-- Retrieve only two or one posts from posts variable -->
<?php $posts_showcase = array_slice($posts, 0, 1); ?>
	<title>DevShots | Home </title>
<style>
.footer{
	color:white;
	background-color:transparent;
	border:none;
}
</style>
</head>
<script src="<?php echo BASE_URL . 'js/mainpage.js'?>">
</script>
<body>
	<!-- container - wraps whole page -->
	<div class="container">
            <!-- // navbar -->
            <!-- banner -->
        <?php include('includes/banner.php') ?>
        <div class="main-page-content">
			<?php foreach($posts_showcase as $post):?>
			<a class="post-link" href="single_post.php?post-slug=<?php echo $post['slug']; ?>">
			<div class="post-header">
				<h2> <?php echo $post['title'] ?> </h2>
				<p> 
				<?php 
					$body = html_entity_decode($post['body']);
					$description = (strlen($body) > 13) ? substr($body,0,30).'...' : $body; 
					echo $description;
				?>
				</p>
			</div>
			</a>
            <?php endforeach ?>
        </div>
		<!-- // Page content -->
	</div>
	<!-- // container -->
    <!-- Footer -->
	<?php include('includes/footer.php'); ?>
    <!-- // Footer -->
